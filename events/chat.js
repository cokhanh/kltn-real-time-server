const users = [];

const addUserChat = ({ id, name, room }) => {
  //co khanh = cokhanh
  console.log(id, name, room);
  if (name && room) {
    name = name.trim().toLowerCase();
    room = room.trim().toLowerCase();
    const existingUser = users.find(
      (user) => user.name === name && user.room === room
    );
    const user = { id, name, room };
    // console.log(user);
    users.push(user);
    return user;
  }
};

const removeUserChat = (id) => {
  const index = users.findIndex((user) => user.id === id);
  if (index !== -1) {
    return users.splice(index, 1)[0];
  }
};

const getUserChat = (name) => users.find((user) => user.name === name);
const getUserComment = (name, room) => users.find((user) => user.name === name && user.room === room);

const getUsersInRoomChat = (room) => users.filter((user) => user.room === room);

module.exports = {
  addUserChat,
  removeUserChat,
  getUserChat,
  getUsersInRoomChat,
  getUserComment
};

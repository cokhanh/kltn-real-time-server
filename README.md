# kltn-real-time-server
- Mở project này lên: npm install
- Mở project client lên (web/native): native đang làm rồi nên bạn chỉ làm web thử coi nhận không. Mở lên xong thì cài package sau: npm install socket.io-client (cái này hình như có cài rồi nếu có sẵn từ học kì trước rồi thì uninstall đi rồi cài lại)
- Vào file gọi socket io chat (tạm thời như vậy sau này sẽ move ra ngoài app để chạy được noti trên web):

```
import io from "socket.io-client";

//đối với functional component
const socket = io("http://192.168.1.185:5000");

//đối với class component thì thêm dòng sau vào constructor
this.socket = io("http://192.168.1.185:5000");
```
Lưu ý: nhớ đổi địa chỉ IP cho phù hợp port dùng 5000
- Chạy server bằng: node index.js

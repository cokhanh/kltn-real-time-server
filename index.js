var express = require("express");
var app = express();
var server = require("http").Server(app);
var io = require("socket.io")(server,{
    cors:{
        origin: "*",
        methods: ["GET", "POST", "PUT", "DELETE"]
    }
});

const {
  addUser,
  getUser,
  getUsersInRoom,
  removeUser,
} = require("./events/notifications");

const { addUserChat, removeUserChat, getUserChat, getUserComment } = require("./events/chat");

// app.use(cors());
server.listen(5000);

io.on("connection", (socket) => {
  console.log("new connection!", socket.id);

  /**
   * event user access
   * when user start the app, server listen this event to add user to his/her rooms
   * purpose: receive notification without the first action 'like'
   */
  socket.on("user-access", function (data, callback) {
    const user = addUser({
      id: socket.id,
      name: data.name,
      room: data.room,
    });

    socket.join(user.room);

    callback();
  });

  /**
   * socket events for notifications
   */

  //listen client liked then push noti
  socket.on("client-liked-status", (data) => {
    const user = getUser(data.current_user_name);
    if (user) {
      socket.to(user.room).emit("server-popup-notification", data);
      socket.emit("server-push-notification", data);
    }
  });

  //create subscription for user when he becomes a friend with someone
  socket.on("subscribe-friend-chanel", function (data, callback) {
    const user = addUser({
      id: socket.id,
      name: data.name,
      room: data.room,
    });

    socket.join(user.room);

    callback();
  });

  // socket event for chat
  //user join in to chat
  // (user open detail chat on react native, user get room by link on web base)
  socket.on("join", function (data, callback) {
    const user = addUserChat({
      id: socket.id,
      name: data.name,
      room: data.room,
    });
    socket.join(user.room);
    callback();
  });

  //send a messages
  socket.on("sendMessage", (message, name, avatar, callback) => {
    const user = getUserChat(name);
    io.to(user.room).emit("message", {
      user: user.name,
      text: message,
      avatar: avatar,
    });
    callback();
  });

  //send a comment
  socket.on("sendComment", (comment, userName, linkProfile, avatar, room, callback) => {
    const user = getUserComment(linkProfile, room);
    io.to(user.room).emit("comment", {
      id: user.id,
      linkProfile: user.name,
      userName,
      comment,
      avatar,
      room
    });
    callback();
  });
});
